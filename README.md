## Icinga, Nagios, ... monitoring plugins
This is a small collection of some monitoring plugins I wrote. This collection
is regularly expanded so visit me again soon.

You could read more details in my personal blog on 
[https://Gehirn-Mag.Net/](https://gehirn-mag.net/) (German only). These plugins 
are tested with icinga2 but should also run on nagios, shinken and other 
monitoring systems which use nagios style plugins.

### Linux

#### check_extmem
An extended memory checker for Linux using physical memory and swap togehter in
one check. Included is a sample icinga2 config and a pnp4nagios template.

![pnp check_extmem](https://gehirn-mag.net/wp-content/uploads/2019/02/extmem-perfdata.png)

#### LSI megaraid, Dell PERC, ...
A simple check for that RAID hba without its own check command.

![megaraid](https://gehirn-mag.net/wp-content/uploads/2019/02/check_megaraid.png)

### Nextcloud

#### check_nextcloud
A simple update checker for nextcloud. Creates CRITICAL alerts if a new update
is available. This checker uses the same update mechanism as nextcloud itself.
There is also a sample config for icinga2.

![check_nextcloud](https://gehirn-mag.net/wp-content/uploads/2019/01/Bildschirmfoto_2019-01-03_07-48-12-1.png)
