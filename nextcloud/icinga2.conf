// A simple icinga2 configuration just to show how that plugin could be used ;-)


// nextcloud check which queries the nextcloud update server
object CheckCommand "check_nextcloud" {
    import "plugin-check-command"
    command = [ PluginDir + "/sits/check_nextcloud.py" ]
    arguments = {
        "-f" = {
            value = "$nextcloud_file$"
            required = true
            description = "Full path to version.php"
        }
        "-c" = {
            value = "$nextcloud_config$"
            required = true
            description = "Full path to config.php"
        }        
        "-p" = {
            value = "$nextcloud_php$"
            description = "Your local php installation if not /usr/bin/php"
        }
        "--updateServer" = {
            value = "$nextcloud_updateserver$"
            description = "The nextcloud update server URL"
        }
    }
    
    // default values
    vars.nextcloud_php = "/usr/bin/php"
    vars.nextcloud_updateserver = "https://updates.nextcloud.org/updater_server/"
}
// apply nextcloud service and check twice a day
apply Service "nextcloud_" for (ncname => ncpath in host.vars.nextcloud) {
    import "generic-service"
    check_command = "check_nextcloud"
    display_name = "Nextcloud " + ncname
    
    check_interval = 12h
    retry_interval = 12h
    max_check_attempts = 1
    enable_perfdata = false
    
    vars.nextcloud_file = ncpath + "/version.php"
    vars.nextcloud_config = ncpath + "/config/config.php"
}
// assign servicegroup nextcloud to all nextcloud installations
object ServiceGroup "nextcloud" {
    display_name = "Nextcloud"
    assign where host.vars.nextcloud && match("nextcloud_*", service.name)
}



// a sample host

object Host "mywebserver"
    ...
    vars.nextcloud = {
        "cloud.customer1.de" = "/var/www/customer1/htdocs",
        "cloud.customer2.de" = "/var/www/customer2/htdocs",
        ...
    }
}


// The visudo line for running the above check
nagios ALL=(root) NOPASSWD: /usr/lib/nagios/plugins/sits/check_nextcloud.py

