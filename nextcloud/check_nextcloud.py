#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# a simple nextcloud version checker ;-)
#

#
# MIT License
#
# Copyright (c) 2018 Steffen Schoch <mein@gehirn-mag.net>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#


# 2018-12-25, schoch: Init...
# 2019-08-26, schoch: read update channel out of config.php


import argparse
import json
import os
import re
import subprocess
import urllib.parse
import urllib.request


# a simple exit function in nagios plugin style
# ARG1: Exitcode
# ARG2: text to print
def myExit(code, text):
    """ exits with nagios plugin style """
    print(text)
    exit(code)


# get arguments
parser = argparse.ArgumentParser(
    description='A simple Nextcloud-Update Check'
)
parser.add_argument(
    '-f', '--file', 
    required = True, 
    help = 'full path to version.php'
)
parser.add_argument(
    '-c', '--config', 
    required = True, 
    help = 'full path to config.php (in most cases something like .../config/config.php)'
)
parser.add_argument(
    '-p', '--php',
    default = '/usr/bin/php',
    help = 'full path to your php binary (default /usr/bin/php)'
)
parser.add_argument(
    '--updateServer',
    default = 'https://updates.nextcloud.org/updater_server/',
    help = 'Base URL for Update Server (default https://updates.nextcloud.org/updater_server/)'
)
parser.add_argument(
    '-v', '--verbose',
    action = 'store_const',
    const = 'verbose',
    help = 'be more verbose'
)
args = parser.parse_args()
# debug
if args.verbose:
    print('args=' + str(args));


# check if version.php is readable
if not os.path.isfile(args.file):
    myExit(3, 'UNKNOWN version.php not found')
# check if php binary exists
if not os.path.isfile(args.php):
    myExit(3, 'UNKNOWN php binary not found')
try:
    ncVersion = json.loads(subprocess.check_output([
        args.php, '-r',
        'include \'' + args.file + '\';include \'' + args.config + '\';' + '''
        $g = array();
        foreach($GLOBALS as $key => $val) {
          if(preg_match('/^(OC_|CONFIG$)/', $key)) {
            $g[$key] = $val;
          }
        }
        echo json_encode(
          array(
            'pv'   => PHP_VERSION,
            'pmav' => PHP_MAJOR_VERSION,
            'pmiv' => PHP_MINOR_VERSION,
            'prv'  => PHP_RELEASE_VERSION,
            'g'    => $g
          )
        );
        '''
    ]))
except:
    myExit(2, 'CRITICAL Cannot parse file ' + args.file)
# debug
if args.verbose:
    print('ncVersion=' + str(ncVersion))


# detect channel
try:
    channel = ncVersion['g']['CONFIG']['updater.release.channel']
except:
    channel = ncVersion['g']['OC_Channel']
if args.verbose:
    print('channel=' + channel)



# create updateURL
updateURL = args.updateServer + '?version=' \
    + 'x'.join(map(str, ncVersion['g']['OC_Version'])) \
    + 'xxx' + channel + 'xx' \
    + urllib.parse.quote_plus(ncVersion['g']['OC_Build']) \
    + 'x' + 'x'.join(map(str, [
        ncVersion['pmav'], ncVersion['pmiv'], ncVersion['prv']
    ]))
if args.verbose:
    print('updateURL=' + updateURL)


# request update information via curl
try:
    req = urllib.request.Request(
        updateURL,
        data = None,
        headers = {
            'User-Agent': 'Nextcloud Updater'
        }
    )
    f = urllib.request.urlopen(req)
except:
    myExit(3, 'Cannot connect to update server')
xml = f.read().decode('utf-8')
if args.verbose:
    print('xml=' + xml)


# xml empty? No updates found
if not xml:
    myExit(
        0, 
        'OK no updates for ' \
        + ncVersion['g']['OC_VersionString'] \
        + ' (channel=' + channel \
        + ' php=' + ncVersion['pv'] + ')'
    )


# check for new version if xml is set
newVersion = ''
if xml:
    matches = re.search('<version>([\d\.]+)', xml)
    if matches:
        newVersion = matches[1]
        myExit(2, 'CRITICAL update available: ' + newVersion \
        + ' (channel=' + channel \
        + ' php=' + ncVersion['pv'] + ')'
        )


# fallback if xml is in strange format
myExit(3, 'UNKNOWN didn\'t understand update server')
