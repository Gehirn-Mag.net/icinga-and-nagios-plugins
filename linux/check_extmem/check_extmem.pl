#!/usr/bin/perl -w

#
# An extended memory check for Linux which watches physical and swap memory.
#

#
# MIT License
#
# Copyright (c) 2015 Steffen Schoch <mein@gehirn-mag.net>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

# 2019-02-15, schoch: Init...


use strict;
use warnings;

use Getopt::Long;


# Nagios Result Codes
use constant {
  STAT_OK       => 0,
  STAT_WARNING  => 1,
  STAT_CRITICAL => 2,
  STAT_UNKNOWN  => 3
};


# Check for arguments
my $help = 0;
my $warning = 0;
my $critical = 0;
GetOptions('help' => \$help, 'warning=i' => \$warning, 'critical=i' => \$critical);
myExit(STAT_UNKNOWN, $0 . ' [ -w <free in percent> -c <free in percent> ]' . "\n") if $help;

# read meminfo, recalculate some values to bytes
my $meminfo = {};
open FH, '< /proc/meminfo'
  or myExit(STAT_CRITICAL, 'Can\'t open memstat: ' . $!);
while(my $line = <FH>) {
  if($line =~ m/^(\S+):\s+(\d+)( (\S+))?$/o) {
    my $key = $1;
    my $val = $2;
    my $unit = lc ($4 || 'b');
    if($unit eq 'b') {
      # Sind bytes, nix tun...
    } elsif($unit eq 'kb') {
      $val *= 1024;
    } elsif($unit eq 'mb') {
      $val *= 1024 * 1024;
    } else {
      chomp $line;
      myExit(STAT_CRITICAL, 'Unknown meminfo unit "' . $unit . '" (' . $line . ')');  
    }
    # recalc always to gb
    $meminfo->{$key} = sprintf '%0.2f', $val / 1073741824;
  }
}
close FH;

# Debug
#use Data::Dumper;
#print Dumper $meminfo;
#map { printf "%15s: %13d\n", $_, $meminfo->{$_} } 
#  qw (MemTotal MemFree Shmem Buffers Cached SwapTotal SwapFree);

# collect values for performance data
my $perfData = {
  'pUsed'   => sprintf('%0.2f', $meminfo->{'MemTotal'} - $meminfo->{'Buffers'} - $meminfo->{'Cached'} - $meminfo->{'MemFree'}),
  'pFree'   => $meminfo->{'MemFree'},
  'pBuffer' => $meminfo->{'Buffers'},
  'pCached' => $meminfo->{'Cached'},
  
  'sUsed'   => sprintf('%0.2f', $meminfo->{'SwapTotal'} - $meminfo->{'SwapFree'}),
  'sFree'   => $meminfo->{'SwapFree'}
};

# calculate and generate used ram as percent
my $sum = 0;
my $free = 0;
$sum += $perfData->{$_} foreach(keys %$perfData);
$free += $perfData->{$_} foreach(qw(pFree pBuffer pCached sFree));
my $percent = sprintf '%d', $free / $sum * 100;

# check status code
my ($statusText, $statusCode) = 
  $percent < $critical 
  ? ('CRITICAL', STAT_CRITICAL) 
  : (
    $percent < $warning ? ('WARNING', STAT_WARNING) : ('OK', STAT_OK)    
  );

# output and finish
myExit(
  $statusCode,
  sprintf('%s %d%% free (w%d/c%d): ', $statusText, $percent, $warning, $critical)  
  . join('|', (join ' ', map { $_ . '=' . $perfData->{$_} } qw(pUsed pFree pBuffer pCached sUsed sFree)) x 2), "\n"
); 

###### functions ###############################################################

# myExit: nagios style exit function
# ARG1: Exit-Code
# ARG2: Text
sub myExit {
  my $exitCode = shift;
  $exitCode = 3 unless defined $exitCode;
  my $exitText = shift || '-UNKNOWN EXIT TEXT - SOMEBODY DIDN\'T KNOW HOW TO USE ME...';
  print $exitText;
  exit $exitCode;
}
