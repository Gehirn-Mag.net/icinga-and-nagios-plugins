## check_extmem.pl

An extended memory checker for Linux using physical memory and swap togehter in
one check. It uses the following formula to calculate the free ram:

free = (physical used + swap used) - (physical free + swap free +
buffers + disk cache)


```
./check_extmem.pl [ -w <free in percent> -c <free in percent> ]
```

There is also an icinga2 sample config and a template for pnp4nagios included.

![pnp check_extmem](https://gehirn-mag.net/wp-content/uploads/2019/02/extmem-perfdata.png)

You will find more details in my personal blog: [https://gehirn-mag.net/monitoring-linux-extended-memory/](https://gehirn-mag.net/monitoring-linux-extended-memory/) (German only).

