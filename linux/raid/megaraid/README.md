## LSI megaraid

A simple check for LSI MegaRAID, Dell PERC H730 or whatever name this adapter has ;-)

First the MegaRAID Storcli needs to be downloaded and installed (in the Downloads area): [[https://www.broadcom.com/products/storage/raid-controllers/megaraid-sas-9271-8i#downloads]([https://www.broadcom.com/products/storage/raid-controllers/megaraid-sas-9271-8i#downloads)


Since I check via NRPE, the following simple command is sufficient:

```
command[check_megaraid]=[ $(echo $(sudo /opt/MegaRAID/MegaCli/MegaCli64 -CfgDsply -aALL -nolog | grep "^State" | cut -d: -f2)) == "Optimal" ] && (echo "RAID Status Optimal"; exit 0) || (echo "RAID Problem"; exit 2)
```
It should be easy to adopt this command to an icinga CheckCommand or anything
else you need instead. 

Add the following line to your nrpe.cfg:
```
nagios ALL=(ALL) NOPASSWD: /opt/MegaRAID/MegaCli/MegaCli64 -CfgDsply -aALL -nolog
```

That was it. Enough completely without even having its own check command.

Note: If you have two arrays you have to adjust everything a bit ;-)

![megaraid](https://gehirn-mag.net/wp-content/uploads/2019/02/check_megaraid.png)
